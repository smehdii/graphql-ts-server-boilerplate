import { createTypeormConn } from "../../utils/createTypeormConn";
import { User } from "../../entity/User";
import { Connection } from "typeorm";
import { TestClient } from "../../utils/TestClient";

let conn: Connection;
const email = "bob51@bob.com";
const password = "jlkajoioiqwe";

let userId: string;

beforeAll(async () => {
  conn = await createTypeormConn();
  const user = await User.create({
    email,
    password,
    confirmed: true
  }).save();
  userId = user.id;
});
 
afterAll(async () => {
  conn.close();
});


describe("logout", () => {


  const sess1  = new TestClient(process.env.TEST_HOST as string)
  const sess2  = new TestClient(process.env.TEST_HOST as string)

  test("test logging out multisessions", async () => {
    
    // Device 1
    await sess1.login(email, password)

    // Device 2
    await sess2.login(email, password)

    expect(sess1.me()).toEqual(sess2.me())

    await(sess1.logout())


    expect(sess1.me()).toEqual(sess2.me())
  })
  
  
  test("test logging out a single user", async () => {
    const client = new TestClient(process.env.TEST_HOST as string)

    await client.login(email, password)

    await client.logout()

    const response2 = await client.me()
    
    expect(response2.data.me).toBeNull();


  });
});