import { invalidLogin, confirmEmailError } from "./errorMessages";
import { User } from "../../entity/User";
import { createTypeormConn } from "../../utils/createTypeormConn";
import { Connection } from "typeorm";
import { TestClient } from "../../utils/TestClient";

const email = "tom@bob.com";
const password = "jalksdf";


let conn: Connection;
beforeAll(async () => {
  conn = await createTypeormConn();
});
afterAll(async () => {
  conn.close();
});

const loginExpectError = async (
  e: string, 
  p: string, 
  errMsg: string,
  client : TestClient

) => {
  const response = await client.login(e,p)

  expect(response.data).toEqual({
    login: [
      {
        path: "email",
        message: errMsg
      }
    ]
  });
};

describe("login", () => {

  const client = new TestClient(process.env.TEST_HOST as string)

  test("email not found send back error", async () => {
    await loginExpectError("bob@bob.com", "whatever", invalidLogin , client);
  });

  test("email not confirmed", async () => {
    await client.register(email, password)

    await loginExpectError(email, password, confirmEmailError , client);

    await User.update({ email }, { confirmed: true });

    await loginExpectError(email, "aslkdfjaksdljf", invalidLogin , client);

    const response = await client.login(email, password)

    expect(response.data).toEqual({ login: null });
  });
});